#!/home/jmhite/anaconda/bin/python

"""
Solve the spring-mass system, 
z'' + Cz' + kZ = 0
z(0)=2, z'(0)=0

subject to: C^2 - 4K < 0 =>
z(t) = 2exp(-Ct/2)cos(sqrt(K-C^2/4)t)
"""

import numpy as np
t = np.linspace(0, 5, 500)
k = 20.5
def f(c):
    z = 2 * np.exp(-c * t / 2) * np.cos(np.sqrt(k - (c ** 2) / 4) * t)
    return z

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('inf')
    parser.add_argument('outf')

    args = parser.parse_args()

#Read parameters from DAKOTA...
    with open(args.inf) as thefile:
        contents = thefile.read()
        contents = contents.split('\n')

    nvars = int(contents[0].lstrip().split()[0])

    evars = map(lambda x: x.lstrip().split(), contents[1:nvars + 1])
    xd = np.array(map(lambda x: np.float64(x[0]), evars))
    labels = map(lambda x: x[1], evars)

    y = f(xd[0])

#Return to DAKOTA
    np.savetxt(args.outf, y)
